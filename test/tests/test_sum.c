#include "sum.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

#define maxVal 2147483647
#define minVal -2147483648

void setUp(void)
{
    /* set stuff up here */
}

void tearDown(void)
{
    /* clean stuff up here */
}

void test_sum_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_sum(0, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_sum(0, 1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_sum(1, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_sum(-1, 1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_sum(-1, 0), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-2, integer_sum(-1, -1), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(25, integer_sum(10, 15), "Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-50, integer_sum(100, -150), "Error in integer_sum");
    
    //tests with max / min value
    TEST_ASSERT_EQUAL_INT_MESSAGE(-1,integer_sum(maxVal,minVal),"Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(maxVal,integer_sum(maxVal,0),"Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(minVal,integer_sum(minVal,0),"Error in integer_sum");
    TEST_ASSERT_EQUAL_INT_MESSAGE(minVal,integer_sum(maxVal,1),"Error in integer_sum");
    
}

